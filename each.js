export const each = (elements, cb) => {
    // this function will iterate over all the elements and returns 
    // element, index and arr to the function defined by user for the array inp.
    // Value, Key and object for Object.

    if (Array.isArray(elements)) {
        for (let elem = 0; elem < elements.length; elem++) {
            cb(elements[elem], elem, elements);
        }
    } else if (typeof (elements) == 'object') {
        let key_arr = Object.keys(elements);
        for (const elem of key_arr) {
            cb(elements[elem], elem, elements);
        }
    }


}

