export const flatten = (elements, output = []) => {
    // this will iterate over all the array
    // if the element is number then push to output array.
    // else recursively pass the arrays into the flatten function.

    for (let elem = 0; elem < elements.length; elem++) {

        if (typeof (elements[elem]) == 'number') {
            output.push(elements[elem])
        } else {
            flatten(elements[elem], output)
        }
    }
    return output;
}
