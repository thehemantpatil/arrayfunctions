export const reduce = (elements, cb, startingValue = null) => {
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

    if (startingValue == null) {
        startingValue = elements[0];
    }

    for (let elem = 0; elem < elements.length; elem++) {
        startingValue = cb(startingValue, elements[elem], elem, elements);

    }
    return startingValue;
}