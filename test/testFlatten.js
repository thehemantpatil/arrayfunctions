import { flatten } from "../flatten.js";

const testFlatten = () => {
    // pass the array to flatten function which you want to convert into single array.
    
    let output = flatten([1, [2], [[3]], [[[4]]]]);
    console.log(output)
}

testFlatten();