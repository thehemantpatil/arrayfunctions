import { each } from '../each.js'

const testEach = () => {
    // test function for each 
    // it takes pass 3 arguments to the function defined.
    // Arguments are element, index and array.

    each([1, 2, 3, 4], (elem, index, arr) => { 
        console.log(elem, index, arr) });
}
testEach();