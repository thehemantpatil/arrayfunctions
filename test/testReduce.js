import { reduce } from "../reduce.js";


const testReduce = () => {
    // test function for each 
    // it takes pass 3 arguments to the function defined.
    // Arguments are element, index and array.

    let output = reduce([1, 2, 3, 4], (memo, num) => {
        return memo + num
    }, 10);
    console.log(output)
}
testReduce();