import { map } from "../map.js";

const testMap = () => {
    // test function for each 
    // it takes pass 3 arguments to the function defined.
    // Arguments are element, index and array.

    let arr = map([1, 2, 3, 4], (num) => {
        return num * 2
    });
    console.log(arr);
}
testMap();