import { filter } from "../filter.js";

const testFilter = () => {
    // pass the array to filter function.
    // second parameter as a function according to user condition.
    // It will filter and return array of the element which are the 
    // satisfy the condition in fucntion.

    let output = filter([1, 2, 3, 4], (num) => {
        return num % 2 == 0
    });
    console.log(output)
}
testFilter();