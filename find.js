export const find = (elements, cb) => {
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

    for (let elem = 0; elem <elements.length; elem++) {
        if(cb(elements[elem])){
            return elements[elem]
        }
    }
    

}