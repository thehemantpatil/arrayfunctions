export const filter = (elements, cb) => {
    // It will return an array of all elements that passed the truth test.
    // Return an empty array if no elements pass the truth test.
    let output = []
    for (let elem = 0; elem<elements.length; elem++) {
        if(cb(elements[elem])){
            output.push(elements[elem])
        }
    }
    return output
}