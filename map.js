export const map = (elements, func) => {
    // It will creates a new array of values by mapping each value in list 
    //through function (iteratee). 
    //The iteratee is passed three arguments: the value, then the index (or key) of the iteration, and finally a reference to the entire list.

    for (let elem = 0; elem < elements.length; elem++) {
        elements[elem] = func(elements[elem], elem, elements)
    }

    return elements;

}

